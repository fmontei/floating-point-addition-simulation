#include <stdio.h>
#include <math.h>
#include <stdlib.h>

unsigned int simulated_fp_add( unsigned int n1, unsigned int n2 )
{
	if (n1 == 0 && n2 == 0) return 0;
	else if (n1 == 0) return n2;
	else if (n2 == 0) return n1;
	
	int exp1, fraction1, significand1;
	int exp2, fraction2, significand2;
	int diff, bigger_exponent;

	exp1      = (n1 >> 23) & 0x000000FF; 
	exp1 -= 127;
	fraction1 =  n1        & 0x007FFFFF; 
	
	exp2      = (n2 >> 23) & 0x000000FF; 
	exp2 -= 127;
	fraction2 =  n2        & 0x007FFFFF;

	fraction1 <<= 6;
	fraction2 <<= 6;

	if (exp1 > exp2) {
		diff = exp1 - exp2;
	}
	else {
		diff = exp2 - exp1;
	}

	if (exp1 > exp2) {
		significand1 = (0x20000000 | fraction1);
		significand2 = (0x20000000 | fraction2) >> diff;
		bigger_exponent = exp1;
	}
	else {
		significand1 = (0x20000000 | fraction1) >> diff;
		significand2 = (0x20000000 | fraction2);
		bigger_exponent = exp2;
	}

	int result_mantissa = (significand1 + significand2);

	// If the MSB of both significands is a 1, then a 1 will carry over into
    	// the bit to the left of the MSB; if this happens, shift mantissa
    	// and compensate by increasing the exponent by 1
	int compare = result_mantissa & 0x3FFFFFFF;
	if (result_mantissa != compare) {
		result_mantissa = result_mantissa >> 1;
		bigger_exponent = bigger_exponent + 1;
	}
	// Remove the hidden bit
	result_mantissa = result_mantissa & 0x1FFFFFFF;

	int result_exponent = 127 + bigger_exponent;
	result_exponent = result_exponent << 23;

	if ((result_mantissa & 0x00000020) == 0x00000020) {
		int check = result_mantissa & 0x0000007F;

		result_mantissa >>= 6;
		result_mantissa += 1;
		if (check == 0x00000020) { result_mantissa -= 1; }	
	}
	else
		result_mantissa >>= 6;

	int result = result_exponent | (result_mantissa);
	
	return result;
}
