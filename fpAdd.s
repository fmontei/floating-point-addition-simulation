/*=============================================================*
    Felipe Monteiro
    fmontei
    CPSC 2310, section 1
    4/18/2014
    Project 5: IEEE 32-bit floating point addition simulation
/*=============================================================*

/*  divide routine
 *
 *  input parameter:
 *    r0 - first number
 *    r1 - second number
 *
 *  return value:
 *    r0 - hexadecimal representation of floating point sum
 *
 *  other output parameters:
 *    none
 *
 *  effect/output
 *    none apart from return value
 *
 *  local register usage: 
 *    r0  - holds first incoming value; otherwise return value
 *	  r1  - holds second incoming value
 *    r2  - (result_mantissa) - holds mantissa for return value
 *    r3  - (result_fraction) - holds fraction for return value
 *    r4  - (exp1)            - holds extracted exponent from r0
 *    r5  - (fraction1)       - holds extracted fraction from r0
 *    r6  - (significand1)    - represents significand for r0
 *    r7  - (exp2)            - holds extracted exponent from r1
 *    r8  - (fraction2)       - holds extracted fraction from r1
 *    r9  - (significand2)    - represents significand for r1
 *    r10 - (mask)            - represents hexadecimal value 0x007FFFFF
 *                              because the compiler did not allow the
 *                              value to be directly loaded into a reigster
 *    r10 - (diff)            - holds the difference between the two exponents;
 *                              used for calculating alignment LSRs
 */

.global simulated_fp_add
.type simulated_fp_add, %function

@ #0x007FFFFF did not work with ldr or mov, so I embedded it
@ here in order to use ldr with the label big_constant
big_constant:	
	.word 0x007FFFFF

simulated_fp_add:
	push {r4, r5, r6, r7, r8, r9, r10, lr}
	result_mantissa .req r2
	result_exponent .req r3
	exp1            .req r4
    	fraction1       .req r5 
	significand1    .req r6
	exp2            .req r7
    	fraction2       .req r8
    	significand2    .req r9
	mask            .req r10

check_for_zeros:
	cmp r0, #0
	beq zero1
	cmp r1, #0
	beq zero2

	b compute

zero1:
	cmp r1, #0
	beq both_zero
	mov r0, r1
	b   end

zero2:
	b   end

both_zero:
	mov r0, #0
	b   end

compute:
	ldr mask, big_constant

	@ Extract exponent from 1st number
	mov exp1, r0, lsr #23
	and exp1, exp1, #0x000000FF
	sub exp1, exp1, #127

	@ Extract fraction from 1st number
	mov fraction1, r0
	and fraction1, fraction1, mask

	@ Extract exponent from 2nd number
	mov exp2, r1, lsr #23
	and exp2, exp2, #0x000000FF	
	sub exp2, exp2, #127

	@ Extract fraction from 2nd number
	mov fraction2, r1
	and fraction2, fraction2, mask

	@ Shift fraction leftward to increase precision 
    	@ by providing more space on the right for addition
	lsl fraction1, fraction1, #6
	lsl fraction2, fraction2, #6

	@ Rename r10 to provide it with new meaning
	.unreq mask
	diff .req r10

	@ Calculate difference
	cmp exp1, exp2
	bgt if1
	ble else1

if1:
	sub diff, exp1, exp2
	b   compute2

else1:
	sub diff, exp2, exp1	

compute2:
	@ Alignment step: 
    	@ Align significand w/ the lower corresponding exponent
	cmp exp1, exp2
	bgt if2
	ble else2

if2:
	@ Add the leftmost invisible 1 to the significands
    	@ Then LSR the significand w/ the lower exponent
    	@ by the difference between the exponents
	orr significand1, fraction1, #0x20000000
	orr significand2, fraction2, #0x20000000
	mov exp2, exp1
	lsr significand2, significand2, diff
	b   compute3

else2:
	orr significand1, fraction1, #0x20000000
	lsr significand1, significand1, diff
	orr significand2, fraction2, #0x20000000
	mov exp1, exp2
	
compute3:
	@ Add the two mantissas together
	add result_mantissa, significand1, significand2

	@ Post-normalization step:
    	@ Check if result_mantissa's last bit carried over too far
	and r1, result_mantissa, #0x3FFFFFFF
	cmp result_mantissa, r1
	bne if3
	b   compute4

if3:
	@ Normalize mantissa, but exponent must be incremented
	lsr result_mantissa, result_mantissa, #1
	add exp1, exp1, #1

compute4:
	temp1 .req r1
	.unreq diff
	temp2 .req r10

    	@ Erase the invisible one, since it is no longer needed
	and result_mantissa, result_mantissa, #0x1FFFFFFF

	@ Convert the fraction back to the original form
	add result_exponent, exp1, #127
	@ Likewise, put the fraction back where it was originally
	lsl result_exponent, result_exponent, #23

	@ Check if a trailing one exists; if so, increment the mantissa by 1
	and temp1, result_mantissa, #0x00000020
	and temp2, result_mantissa, #0x0000007F

	cmp temp1, #0x00000020
	beq increment_mantissa
	
	@ Otherwise, just LSR the mantissa to properly align it
	lsr result_mantissa, result_mantissa, #6
	b   done

increment_mantissa:
	lsr result_mantissa, result_mantissa, #6
	add result_mantissa, result_mantissa, #1

	cmp temp2, #0x00000020
	beq undo_increment

	b   done

undo_increment:
	sub result_mantissa, result_mantissa, #1

done:
	@ Logical-OR the fraction and mantissa together 
    	@ and store the result in r0, the return value
	orr r0, result_exponent, result_mantissa

end:                        
	pop {r4, r5, r6, r7, r8, r9, r10, pc}
